import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Hero } from './hero';
import { HeroService } from './hero.service';

@Component({
    selector: 'my-hero-detail',
    template: `
        <div *ngIf="hero">
        <h2>{{ hero.name }} detail</h2>
        <div>
            <label>ID: </label>
            {{ hero.id }}
        </div>
        <div>
            <label>NAME: </label>
            <input [(ngModel)]="hero.name">
        </div>
        <button (click)="goBack()">Back</button>
        </div>
        <button (click)="save()">Save</button>
    `,
    styleUrls: ['app/hero-detail.component.css']

})
export class HeroDetailComponent implements OnInit{
    hero: Hero;
    constructor(private heroService:HeroService, private route:ActivatedRoute){}

    ngOnInit():void{
        this.route.params.forEach((params: Params) => {
            let id = params['id'];
            this.heroService.getHero(id).then(hero => this.hero=hero);
            
        })    
    }

    save():void{
        this.heroService.update(this.hero)
            .then(this.goBack)
    }

    goBack():void{
        window.history.back()
    }
}